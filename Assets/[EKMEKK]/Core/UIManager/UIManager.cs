using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{


    public GameObject panelMainMenu;
    public GameObject panelSuccess;
    public GameObject panelFail;
    public GameObject panelInGame;
    GameManager2 manager;

    public TMPro.TMP_Text currentLevel;
    public TMPro.TMP_Text nextLevel;

    private UIManager()
    {

    }

    void Awake()
    {
        panelMainMenu.SetActive(true);
        panelSuccess.SetActive(false);
        panelFail.SetActive(false);
        manager = GetComponent<GameManager2>();
        currentLevel.text = manager.GetCurrentLevel().ToString();
        nextLevel.text = (manager.GetCurrentLevel() + 1).ToString();

    }

    void Start()
    {
        Debug.Log(PlayerPrefs.GetInt("isFirstGame", 1));
        // E�ER �LK OYUN DE��LSE D�REK BA�LAT
        if (PlayerPrefs.GetInt("isFirstGame", 1) == 0)
        {
            PlayerPrefs.SetInt("isFirstGame", 0);
            EventClickPlay();
        }
    }

    public void EventClickPlay()
    {

        panelMainMenu.SetActive(false);
        panelInGame.SetActive(true);
        manager.PlayButton();

    }
    public void EventClickRetry()
    {
        panelFail.SetActive(false);
        panelInGame.SetActive(true);
        manager.RetryButton();

    }
    public void EventClickContinue()
    {
        panelSuccess.SetActive(false);
        panelInGame.SetActive(true);
        manager.ContuniueButton();


    }
    public void Fail()
    {
        panelFail.SetActive(true);
        panelInGame.SetActive(false);

    }
    public void Success()
    {
        currentLevel.text = manager.GetCurrentLevel().ToString();
        nextLevel.text = (manager.GetCurrentLevel() + 1).ToString();
        panelSuccess.SetActive(true);
        panelInGame.SetActive(false);

    }


}
