using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ballistic : MonoBehaviour
{


    ///<summary>
    ///Does the thing.
    ///</summary>
    ///<param name="angle">Atış açısını belirler.</param>
    ///<param name="children">Ragdoll neslerinin bütün parçalarına aynı ivmeyi uygular.</param>
    ///<param name="objects">Fırlatmak istediğiniz nesne .</param>
    ///<param name="startPosition">Fırlatma başlangıç noktası .</param>
    ///<param name="targetPosition">Fırlatma hedef noktası.</param>
    public static void Throw(Rigidbody objects, Vector3 startPosition, Vector3 targetPosition, float angle, bool children)
    {
        var velocity = BallisticVelocity(targetPosition, startPosition, angle);

        Rigidbody[] list;
        if (children)
        {
            list = objects.GetComponentsInChildren<Rigidbody>();
        }
        else
        {
            list = new Rigidbody[1];
            list[0] = objects;
        }

        for (int i = 0; i < list.Length; i++)
        {
            list[i].velocity = velocity;
        }


    }

    private static Vector3 BallisticVelocity(Vector3 destination, Vector3 startPosition, float angle)
    {
        //Debug.LogError("des: " + destination + " angle: " + angle);
        Vector3 dir = destination - startPosition; // get Target Direction
        float height = dir.y; // get height difference
        dir.y = 0; // retain only the horizontal difference
        float dist = dir.magnitude; // get horizontal direction
        float a = angle * Mathf.Deg2Rad; // Convert angle to radians
        dir.y = dist * Mathf.Tan(a); // set dir to the elevation angle.
        dist += height / Mathf.Tan(a); // Correction for small height differences

        // Calculate the velocity magnitude
        float velocity = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));
        return velocity * dir.normalized; // Return a normalized vector.
    }
}
