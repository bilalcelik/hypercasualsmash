using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;

    UIManager uiManager;
    LevelManager levelManager;
    void Start()
    {
        Instance = this;
        uiManager = GetComponent<UIManager>();
        levelManager = GetComponent<LevelManager>();
        levelManager.CreateLevel();

    }
    public delegate void GameEvent();
    public enum GameState
    {
        GameStart, Btn_ClickPlay, Btn_ClickRetry, Btn_ClickContinue
    }


    Dictionary<GameState, GameEvent> eventList = new Dictionary<GameState, GameEvent>();
    string levelCountKey = "EKMEKK_GAME_LEVEL";

    public bool isStart = false;
    public bool isEndGame;
    public void Success()
    {
        if (!isEndGame)
        {
            Debug.LogWarning("aszfasgasgsdg");
            PlayerPrefs.SetInt(levelCountKey, GetCurrentLevel() + 1);
            uiManager.Success();
            isEndGame = true;
        }

    }

    private bool isRetry;
    private bool isContinue;
    public void Fail()
    {
        isEndGame = true;
        uiManager.Fail();
    }
    public int GetCurrentLevel()
    {
        return PlayerPrefs.GetInt(levelCountKey, 1);
    }

    public void Btn_ClickRetry()
    {
        levelManager.CreateLevel();
        isRetry = true;
        isEndGame = false;


    }
    public void Btn_ClickContinue()
    {
        Time.timeScale = 1;
        levelManager.CreateLevel();
        isContinue = true;
        isEndGame = false;
    }
    public void Btn_ClickPlay()
    {
        isStart = true;
        if (eventList.ContainsKey(GameState.Btn_ClickPlay))
        {
            eventList[GameState.Btn_ClickPlay]();
        }
        if (eventList.ContainsKey(GameState.GameStart))
        {
            eventList[GameState.GameStart]();
        }
    }



    public void Subscribe(GameState listenState, GameEvent func)
    {



        if (!eventList.ContainsKey(listenState))
        {
            eventList.Add(listenState, func);
        }
        else
        {
            eventList[listenState] += func;

            if (listenState == GameState.GameStart && (isRetry || isContinue))
            {



                if (eventList.ContainsKey(GameState.Btn_ClickRetry) && isRetry)
                {
                    eventList[GameState.Btn_ClickRetry]();
                    isRetry = false;
                }
                if (eventList.ContainsKey(GameState.Btn_ClickContinue) && isContinue)
                {
                    eventList[GameState.Btn_ClickContinue]();
                    isContinue = false;
                }
                if (eventList.ContainsKey(GameState.GameStart))
                {
                    eventList[GameState.GameStart]();
                }
            }
        }
    }
}
