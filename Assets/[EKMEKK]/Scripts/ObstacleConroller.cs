using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObstacleConroller : MonoBehaviour
{
    public GameObject orgPart, firstBrokenParts;

    Rigidbody[] childs;

    [Header("Multiple obstacles")]
    [Tooltip("Alttakilerde script OLMASIN!")]
    public GameObject[] beneathObstacles;

    [HideInInspector] public bool isHit = false;
    public bool isLast = false;

    Player player;
    Camera camera;
    Vector3 camFirstPos;

    int beneathIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        childs = firstBrokenParts.transform.GetComponentsInChildren<Rigidbody>();

        player = FindObjectOfType<Player>();

        camera = FindObjectOfType<Camera>();
        camFirstPos = camera.transform.position;

        firstBrokenParts.SetActive(false);

        for (int i = 0; i < beneathObstacles.Length; i++)
        {
            beneathObstacles[i].SetActive(false);
        }

    }


    // Herhangi bir par�aya kontrol edilen de�erse di�er par�alar�n kinemati�i kapan�r ve a�a�� g�� uygulan�r
    public void PlayerHit(string triggeredTag)
    {
        ObstacleManager.instance.UpdateObstacleSpeed();

        player.railHitCheck = false;
        player.currentRailHitTime = player.armSpeed + 0.1f;

        //orgPart.SetActive(false);

        for (int i = 0; i < orgPart.GetComponentsInChildren<MeshRenderer>().Length; i++)
        {
            orgPart.GetComponentsInChildren<MeshRenderer>()[i].enabled = false;
        }

        firstBrokenParts.SetActive(true);
        for (int i = 0; i < beneathObstacles.Length; i++)
        {
            beneathObstacles[i].SetActive(true);
        }


        if (!isHit)
        {

            for (int i = 0; i < childs.Length; i++)
            {

                childs[i].isKinematic = false;
                //childs[i].AddForce(Vector3.down * 10);
                childs[i].velocity = Random.onUnitSphere * 20;
                //childs[i].GetComponent<Collider>().isTrigger = false;
            }

            camera.DOShakePosition(.5f, .3f, 10, 90);
            camera.DOShakeRotation(.5f, .3f, 10, 90).OnComplete(() =>
            {
                //camera.transform.position = camFirstPos;
                camera.transform.DOMove(camFirstPos, .5f);
            }); ;


            // E�er 1'den fazla k�r�lacak obje varsa a�a��ya girer ve di�erlerini de s�rayla k�rar

            int beneathObstacle = beneathObstacles.Length;
            
            if(beneathObstacle > 0 && beneathIndex < beneathObstacle)
            {
                Debug.LogError("1");
                childs = beneathObstacles[beneathIndex].transform.GetComponentsInChildren<Rigidbody>();
                beneathIndex++;
            }
            else
            {
                isHit = true;
                orgPart.SetActive(false);

            }

            if (isLast)
            {
                FindObjectOfType<ObstacleManager>().confetti.SetActive(true);
                GameManager2.instance.Success();

                StartCoroutine(SlowMo(beneathObstacle));

            }

        }
    }

    Vector3 torque;
    IEnumerator SlowMo(int i)
    {
        //Debug.LogError(i);
        yield return new WaitForSeconds(0);

        DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1f, .15f).OnComplete(() =>
        {
            // E�er k�r�lacak objeler bitmediyse zaman� yava�lat�p h�zland�r�p k�rmaya devam eder.
            if (i > 0)
            {
                Rigidbody[] rbs = beneathObstacles[i - 1].GetComponentsInChildren<Rigidbody>();

                for (int j = 0; j < rbs.Length; j++)
                {

                    rbs[j].isKinematic = false;
                    //childs[i].AddForce(Vector3.down * 10);
                    rbs[j].velocity = Random.onUnitSphere * 20;

                }

                camera.DOShakePosition(.5f, .3f, 10, 90);
                camera.DOShakeRotation(.5f, .3f, 10, 90).OnComplete(() =>
                {
                    //camera.transform.position = camFirstPos;
                    camera.transform.DOMove(camFirstPos, .5f);
                });
                //Time.timeScale = .3f;

                i--;

                StartCoroutine(SlowMo(i));
            }


        });

    }
}
