using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Obstacle : MonoBehaviour
{
    public bool isBomb = false;
    ObstacleConroller oc;
    // Start is called before the first frame update
    void Start()
    {
        oc = GetComponentInParent<ObstacleConroller>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Head" || other.tag == "Left" || other.tag == "Right")
        {
            if (!isBomb)
            {
                oc.PlayerHit(other.tag);

                GameObject particle = Instantiate(FindObjectOfType<Player>().starParticle);
                particle.transform.position = transform.position;
            }
            else
                Explode();
        }
        else if(other.tag == "EndCheck" && oc.isLast && !oc.isHit)
        {
            FindObjectOfType<ObstacleManager>().confetti.SetActive(true);
            GameManager2.instance.Success();
        }
        else if(other.tag == "StartCheck")
        {
            GetComponentInParent<ObstacleMovement>().isActive = true;
            GetComponentInParent<ObstacleMovement>().speed = ObstacleManager.instance.currentObstacleSpeed;

            Debug.LogError("111111");
        }
    }

    void Explode()
    {
        FindObjectOfType<Camera>().transform.DOShakePosition(.5f, .3f, 10, 90);
        FindObjectOfType<Camera>().transform.DOShakeRotation(.5f, .3f, 10, 90);

        transform.GetComponent<MeshRenderer>().enabled = false;

        transform.GetChild(0).gameObject.SetActive(true);

        FindObjectOfType<Player>().flash = true;
        FindObjectOfType<Player>().myCG.alpha = 1;

        GameManager2.instance.Fail();
    }
}
