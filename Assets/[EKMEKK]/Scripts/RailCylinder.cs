using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailCylinder : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, 70 * Time.deltaTime*Random.Range(1f,1.25f)); //rotates 50 degrees per second around z axis
    }
}
