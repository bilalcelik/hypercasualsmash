using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    
    public static ObstacleManager instance;

    public GameObject confetti;

    public GameObject[] obstacles;

    [Header("Bomba")]
    public bool shouldBomb = false;
    public GameObject bomb;

    public int objectCountForLevel = 5;
    public float spawnTime = 3f;

    float currentSpawnTime;
    bool spawnLock = true;

    int randomIndex;

    public float currentObstacleSpeed = 2;

    public Action UpdateObstacleSpeedAction;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        //currentSpawnTime = spawnTime;

        objectCountForLevel += PlayerPrefs.GetInt("level", 1);

        if(objectCountForLevel > 15)
        {
            objectCountForLevel = 15;
        }

        currentSpawnTime = 0;
        spawnLock = false;
    }

    void Update()
    {
        if (!spawnLock && GameManager2.instance.isStart)
        {
            currentSpawnTime -= Time.deltaTime;

            if(currentSpawnTime <= 0)
            {
                currentSpawnTime = spawnTime;

                //spawnLock = true;
                SpawnObstacle();
            }

        }
    }

    public void UpdateObstacleSpeed()
    {
        currentObstacleSpeed += .3f;
        UpdateObstacleSpeedAction.Invoke();
    }

    public void ResetObstacleSpeed()
    {
        currentObstacleSpeed = 2;
        UpdateObstacleSpeedAction.Invoke();

    }

    void SpawnObstacle()
    {
        objectCountForLevel--;

        if(objectCountForLevel >= 0)
        {
            GameObject dummy;
            if (shouldBomb && objectCountForLevel != 0)
            {
                float randomPer = UnityEngine.Random.Range(0, 10);

                if(randomPer < 3)
                {
                    dummy = Instantiate(bomb).gameObject;
                }
                else
                {
                    randomIndex = UnityEngine.Random.Range(0, obstacles.Length);

                    dummy = Instantiate(obstacles[randomIndex]).gameObject;
                }
            }
            else
            {
                randomIndex = UnityEngine.Random.Range(0, obstacles.Length);

                dummy = Instantiate(obstacles[randomIndex]).gameObject;
            }
            
            if(objectCountForLevel == 0)
            {
                dummy.GetComponent<ObstacleConroller>().isLast = true;
            }

            Debug.Log("spawn");
        }
        else if(!GameManager2.instance.isEndGame)
        {
            Debug.LogError("kazan�");

            //confetti.SetActive(true);
            //GameManager2.instance.Success();
        }

        
    }
}
