using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using DG.Tweening;

public class Dragger : MonoBehaviour
{

    Player player;
    ChainIKConstraint chainIK;

    // B�rak�ld�ktan sonra ilk pozisyonuna d�nmesini sa�layacak referans
    public GameObject ResetParent;

    // �lk tutuldu�undaki pozisyon
    public Vector3 startPos;

    // Hangi v�cut par�as� oldu�unu belirleyen Enum de�i�ken
    public RigPos rigPos;

    // V�cut par�alar�n�n hareket edebildi�i alanlar
    [Header("Restraints")]
    public GameObject LeftArmRestrain;
    public GameObject HeadRestrain;
    public GameObject RightArmRestrain;

    // V�cut par�alar�n�n hareket edebilece�i alanlar�n k��elerinin pozisyonlar�
    Bounds boundsHead, boundsLeft, boundRight;

    void Start()
    {
        chainIK = GetComponent<ChainIKConstraint>();
        player = FindObjectOfType<Player>();

        boundsHead = HeadRestrain.GetComponent<Renderer>().bounds;
        boundsLeft = LeftArmRestrain.GetComponent<Renderer>().bounds;
        boundRight = RightArmRestrain.GetComponent<Renderer>().bounds;

        previousPosition = ResetParent.transform.position;
    }

    public float theDistance = 0;
    public bool record = true;
    private Vector3 previousPosition;
    public float speed;
    void FixedUpdate()
    {
        if(!GameManager.Instance.isEndGame && GameManager.Instance.isStart)
        {
            theDistance = Vector3.Distance(ResetParent.transform.position, previousPosition);
            previousPosition = ResetParent.transform.position;

            speed = theDistance / Time.deltaTime;

            //Debug.Log(speed);
        }
        
    }

    void OnMouseDown()
    {

        if (!GameManager.Instance.isEndGame && GameManager.Instance.isStart)
        {
            chainIK.weight = 1;

            startPos = transform.position;
        }
    }

    void OnMouseDrag()
    {

        if (!GameManager.Instance.isEndGame && GameManager.Instance.isStart)
        {

            float distance_to_screen = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
            Vector3 pos_move = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance_to_screen));

            //Vector3 offset = pos_move - player.circleCenterObject.position;
            //offset = offset.normalized * 2f;
            //transform.position = offset;

            if (rigPos == RigPos.Head)
            {
                if (pos_move.y < boundsHead.min.y)
                    pos_move = new Vector3(pos_move.x, boundsHead.min.y, pos_move.z);
                else if (pos_move.y > boundsHead.max.y)
                    pos_move = new Vector3(pos_move.x, boundsHead.max.y, pos_move.z);

                if (pos_move.x < boundsHead.min.x)
                    pos_move = new Vector3(boundsHead.min.x, pos_move.y, pos_move.z);
                else if (pos_move.x > boundsHead.max.x)
                    pos_move = new Vector3(boundsHead.max.x, pos_move.y, pos_move.z);

                if (pos_move.z > boundsHead.max.z)
                    pos_move = new Vector3(pos_move.x, pos_move.y, boundsHead.max.z);
                //else if (pos_move.z > zMax.transform.position.z)
                //    pos_move = new Vector3(pos_move.x, pos_move.y, zMax.transform.position.z);
            }
            else if (rigPos == RigPos.leftArm)
            {
                if (pos_move.y < boundsLeft.min.y)
                    pos_move = new Vector3(pos_move.x, boundsLeft.min.y, pos_move.z);
                else if (pos_move.y > boundsLeft.max.y)
                    pos_move = new Vector3(pos_move.x, boundsLeft.max.y, pos_move.z);

                if (pos_move.x < boundsLeft.min.x)
                    pos_move = new Vector3(boundsLeft.min.x, pos_move.y, pos_move.z);
                else if (pos_move.x > boundsLeft.max.x)
                    pos_move = new Vector3(boundsLeft.max.x, pos_move.y, pos_move.z);

                if (pos_move.z < boundsLeft.min.z)
                    pos_move = new Vector3(pos_move.x, pos_move.y, boundsLeft.min.z);
                else if (pos_move.z > boundsLeft.max.z)
                    pos_move = new Vector3(pos_move.x, pos_move.y, boundsLeft.max.z);
            }
            else if (rigPos == RigPos.rightArm)
            {
                if (pos_move.y < boundRight.min.y)
                    pos_move = new Vector3(pos_move.x, boundRight.min.y, pos_move.z);
                else if (pos_move.y > boundRight.max.y)
                    pos_move = new Vector3(pos_move.x, boundRight.max.y, pos_move.z);

                if (pos_move.x < boundRight.min.x)
                    pos_move = new Vector3(boundRight.min.x, pos_move.y, pos_move.z);
                else if (pos_move.x > boundRight.max.x)
                    pos_move = new Vector3(boundRight.max.x, pos_move.y, pos_move.z);

                if (pos_move.z < boundRight.min.z)
                    pos_move = new Vector3(pos_move.x, pos_move.y, boundRight.min.z);
                else if (pos_move.z > boundRight.max.z)
                    pos_move = new Vector3(pos_move.x, pos_move.y, boundRight.max.z);
            }




            transform.position = new Vector3(pos_move.x, pos_move.y, pos_move.z);


        }
    }


    void OnMouseUp()
    {


        DOTween.To(() => chainIK.weight, x => chainIK.weight = x, 0, .5f * (1 / Time.timeScale)).OnComplete(() =>
        {
            transform.position = ResetParent.transform.position;
        });
    }
}

public enum RigPos
{
    leftArm,
    rightArm,
    Head,
}
