using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    Rigidbody[] rbs;
    public bool isChair = false;
    public float speed = 1f;

    [HideInInspector] public bool isActive = false;

    // Start is called before the first frame update
    void Start()
    {
        rbs = GetComponentsInChildren<Rigidbody>();

        ObstacleManager.instance.UpdateObstacleSpeedAction += UpdateSpeed;

        //speed = ObstacleManager.instance.currentObstacleSpeed;
        speed = 6;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isChair)
            transform.Translate(Vector3.left * Time.deltaTime * speed);
        else
            transform.Translate(Vector3.forward * Time.deltaTime * speed);

    }

    void UpdateSpeed()
    {
        if (isActive)
            speed = ObstacleManager.instance.currentObstacleSpeed;
    }


    void Drop()
    {
        for (int i = 0; i < rbs.Length; i++)
        {
            rbs[i].isKinematic = false;
        }
    }
}
