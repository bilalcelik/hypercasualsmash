using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager2 : MonoBehaviour
{
    public static GameManager2 instance;
    UIManager UIManager;

    public bool isLevelManager;

    public bool isStart;
    public bool isEndGame;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        if (isLevelManager)
        {
            PlayerPrefs.SetInt("isFirstGame", 1);
            
            SceneManager.LoadScene(PlayerPrefs.GetInt("levelIndex", 1));

        }
        

        UIManager = GetComponent<UIManager>();
        isStart = false;
        isEndGame = false;
    }

    public void PlayButton()
    {
        isStart = true;
        //FindObjectOfType<Player>().StartRun();
    }

    public void RetryButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    public void ContuniueButton()
    {
        NextLevel();
    }

    public void Fail()
    {
        FindObjectOfType<Player>().isHolding = false;
        isEndGame = true;
        UIManager.Fail();
    }

    public void Success()
    {
        // LEVEL� ARTTIR
        FindObjectOfType<Player>().isHolding = false;
        isEndGame = true;
        UIManager.Success();
    }


    void NextLevel()
    {
        PlayerPrefs.SetInt("level", PlayerPrefs.GetInt("level", 1) + 1);
        PlayerPrefs.SetInt("levelIndex", PlayerPrefs.GetInt("levelIndex", 1) + 1);

        int levelIndex = PlayerPrefs.GetInt("levelIndex", 1);


        if (levelIndex >= SceneManager.sceneCountInBuildSettings)
        {
            PlayerPrefs.SetInt("levelIndex", 1);
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

    }

    public int GetCurrentLevel()
    {
        return PlayerPrefs.GetInt("level", 1);
    }
}
