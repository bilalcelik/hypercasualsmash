using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using System;
using DG.Tweening;

public class Player : MonoBehaviour
{

    public TwoBoneIKConstraint NewArmIK;
    public GameObject armTarget, armTargetEndPos;
    [Space]
    public GameObject starParticle;


    [Space]
    [HideInInspector] public bool isClicked = false, railHitCheck = false, isHolding = false;
    public float railHitTime = .35f;
    public float holdTime = 15f;
    [HideInInspector] public float currentRailHitTime;

    [Header("Flash")]
    public CanvasGroup myCG;
    [HideInInspector] public bool flash = false;

    Camera camera;

    void Start()
    {
        camera = FindObjectOfType<Camera>();

        railHitTime = armSpeed + 0.1f;
        currentRailHitTime = railHitTime;

        //armTarget.transform.position = armTargetEndPos.transform.position;

        currentRailHitFailTime = railHitFailTime;
    }


    bool railHitFail = false;
    float railHitFailTime = 1f, currentRailHitFailTime;

    void Update()
    {
        // TIKLAMA
        if (Input.GetMouseButtonDown(0) && !railHitFail && !isClicked && GameManager2.instance.isStart && !GameManager2.instance.isEndGame)
        {
            Debug.Log("hit");
            isClicked = true;
            railHitCheck = true;
            currentRailHitTime = railHitTime;

            NewArmHit();
            //currentClickTime = clickTime;
        }
        if (Input.GetMouseButton(0) && !railHitFail && isClicked && GameManager2.instance.isStart && !GameManager2.instance.isEndGame)
        {
            //Debug.Log("bas���");
            isHolding = true;
            holdTime += Time.deltaTime;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            //Debug.LogError("kalkt�");
            isHolding = false;
            //isClicked = false;
            holdTime = 15f;
        }

        // BANDA �ARPTI�INI KONTROL EDER, ��ERDEK� �FE �STED���N� YAZAB�L�RS�N
        if (railHitCheck)
        {
            currentRailHitTime -= Time.deltaTime;

            if (currentRailHitTime <= 0)
            {
                railHitCheck = false;
                currentRailHitTime = railHitTime;

                isClicked = true;
                isHolding = false;

                railHitFail = true;

                ObstacleManager.instance.ResetObstacleSpeed();

            }
        }

        if (railHitFail)
        {
            currentRailHitFailTime -= Time.deltaTime;

            if (currentRailHitFailTime <= 0)
            {
                railHitFail = false;

                currentRailHitFailTime = railHitFailTime;

                
            }
        }

        // BOMBAYA BASARSA AKT�F OLUR - Obstacle SCR�PT�NDEN
        if (flash)
        {
            myCG.alpha = myCG.alpha - Time.deltaTime;
            if (myCG.alpha <= 0)
            {
                myCG.alpha = 0;
                flash = false;
            }
        }

    }

    public Ease ease;
    public float armSpeed;
    void NewArmHit()
    {
        armTarget.transform.DOMove(armTargetEndPos.transform.position, armSpeed).SetLoops(2, LoopType.Yoyo).SetEase(ease).OnComplete(() =>
        {
            if (isHolding)
            {
                railHitCheck = true;
                currentRailHitTime = railHitTime;
                NewArmHit();

            }
            else
                isClicked = false;

            if (railHitFail)
            {
                armTarget.transform.DOPunchPosition((Vector3.up + Vector3.forward), .3f, 10, 1f).OnComplete(() =>
                {

                });
            }
        });

       
    }

}
